FROM docker.io/continuumio/miniconda3

RUN apt update && apt install git -yqq && pip install mlflow

ENTRYPOINT ["mlflow"]

CMD ["ui", "-h", "0.0.0.0"]
