import mlflow
import mlflow.sklearn

from wine_quality.run import download_data, prepare_data, eval_metrics

mlflow.set_tracking_uri("http://localhost:5000")
model = mlflow.sklearn.load_model("models:/ElasticnetWineModel/1")

data = download_data()

_, _, test_x, test_y = prepare_data(data)

predicted_qualities = model.predict(test_x)

(rmse, mae, r2) = eval_metrics(test_y, predicted_qualities)

print("Elasticnet model")
print("\tRMSE: %s" % rmse)
print("\tMAE: %s" % mae)
print("\tR2: %s" % r2)
