# ML Flow example

To run the experiment, execute `docker-compose up`. This should build the images and run the experiment once. If you want to run it again, execute `docker-compose start train_oneshot`.

Once trained, run the model by executing `python predict.py`
